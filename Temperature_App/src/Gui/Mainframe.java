package Gui;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import dao.conversionValidate;
import dao.tempdataImpl;
import java.awt.Color;

public class Mainframe {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mainframe window = new Mainframe();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Mainframe() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Temperature Conversion");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setBounds(35, 32, 235, 20);
		frame.getContentPane().add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(33, 79, 142, 50);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBackground(Color.BLACK);
		textField_1.setForeground(Color.WHITE);
		textField_1.setBounds(236, 79, 131, 50);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Fahrenheit", "Celsius"}));
		comboBox.setBounds(35, 129, 140, 20);
		frame.getContentPane().add(comboBox);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBackground(Color.LIGHT_GRAY);
		comboBox_1.setEditable(true);
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"Celsius","Fahrenheit"}));
		comboBox_1.setBounds(236, 129, 131, 20);
		frame.getContentPane().add(comboBox_1);
		

		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				 String selectedBook = (String) comboBox.getSelectedItem();
				 if (selectedBook.equals("Fahrenheit")) {
					 comboBox_1.setSelectedIndex(0);	
		        } else if (selectedBook.equals("Celsius")) {
		        	 comboBox_1.setSelectedIndex(1); 
		        }
				
			}
		});
	
		
		
		JButton btnNewButton = new JButton("Convert");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 String selectedBook = (String) comboBox.getSelectedItem();
				 if (selectedBook.equals("Celsius")) {
					 
					 conversionValidate cvObj = new conversionValidate(); 
					 if(!cvObj.Validateempty(textField.getText())) {
							JOptionPane.showMessageDialog(null, "input place is empty", "Error",
									JOptionPane.ERROR_MESSAGE);
						 
					 }	
					 else if(!cvObj.Validateinput(textField.getText())) {
						 JOptionPane.showMessageDialog(null, "input is not valid", "Error",
									JOptionPane.ERROR_MESSAGE);
						 }
					 else if(!cvObj.ValidatenC(textField.getText())) {
						 JOptionPane.showMessageDialog(null, "input is out of range (Range : -273.15 to 100)", "Error",
									JOptionPane.ERROR_MESSAGE);
						 }
				
					 else {
						
						double Temp = Double.parseDouble(textField.getText());
						 tempdataImpl tdi = new tempdataImpl();
						 double CT= tdi.CtoF(Temp);
						 textField_1.setText(String.valueOf(CT));
					
				 }
					 
		        } else if(selectedBook.equals("Fahrenheit")) {
		        	conversionValidate cvObj = new conversionValidate(); 
					 if(!cvObj.Validateempty(textField.getText())) {
							JOptionPane.showMessageDialog(null, "input place is empty", "Error",
									JOptionPane.ERROR_MESSAGE);
						 
					 }
					 else if(!cvObj.Validateinput(textField.getText())) {
						 JOptionPane.showMessageDialog(null, "input is not valid", "Error",
									JOptionPane.ERROR_MESSAGE);
						 }
				
					 else if(!cvObj.ValidatenF(textField.getText())) {
						 
						 JOptionPane.showMessageDialog(null, "input is out of range (Range : -460 to 212)", "Error",
									JOptionPane.ERROR_MESSAGE);
						 }
					
				
					 else {
		        	
						double Temp = Double.parseDouble(textField.getText());
						 tempdataImpl tdi = new tempdataImpl();
						 double CT= tdi.FtoC(Temp);
						 textField_1.setText(String.valueOf(CT));
					
					 }
		        }
				
				
			}
		});
		btnNewButton.setBounds(163, 183, 89, 23);
		frame.getContentPane().add(btnNewButton);
	}
}
